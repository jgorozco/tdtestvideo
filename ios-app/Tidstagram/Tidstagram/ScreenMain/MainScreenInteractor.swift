import Foundation

class MainScreenInteractor:MainScreenInteractorProtocol{
    
    var videoService:VideoServiceProtocol?
    
    var videos:[VideoEntity]?
    var actualVideo:Int = -1
    
    func loadVideos(interactorCompletion:@escaping(Result<Int,Error>)->Void){
        if let availableVideos = videoService?.loadFromNotification(pushInfo: nil){
            videos = availableVideos
            videoService?.cacheVideos(videos: availableVideos, completion: interactorCompletion)
        }else{
            interactorCompletion(.failure(TidsError.NoVideoError))
        }
    }
    
    func getNextVideo()->VideoEntity?{
        actualVideo += 1
        if (actualVideo>=videos?.count ?? 0){
            actualVideo = 0
        }
        let video = videos?[actualVideo]
        //TODO implement a recall if videopath not exist
        video?.localPath = videoService?.getVideoCachePath(video: video!) ?? ""
        return video
    }
    
    func getAllVideos()->[VideoEntity]?{
        return videos?.map{ thisVideo in
            thisVideo.localPath = videoService?.getVideoCachePath(video: thisVideo) ?? ""
            thisVideo.localThub = videoService?.getVideoCacheThub(video: thisVideo) ?? ""
             return thisVideo
            }.filter{
                $0.localPath != ""
        }
    }
    
}
