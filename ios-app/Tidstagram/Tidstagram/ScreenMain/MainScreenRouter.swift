import Foundation
import UIKit

class MainScreenRouter{
    
    static func createModule()-> MainScreenView{
        let view:MainScreenViewProtocol = mainStoryboard.instantiateInitialViewController() as! MainScreenViewProtocol
        let presenter: MainScreenPresenterProtocol = MainScreenPresenter()
        let interactor: MainScreenInteractorProtocol = MainScreenInteractor()
        let videoService: VideoServiceProtocol = ServiceGetVideo()
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        interactor.videoService = videoService
        return view as! MainScreenView
    }

    static var mainStoryboard:UIStoryboard{
        return UIStoryboard(name:"MainScreen",bundle: Bundle.main)
    }
}
