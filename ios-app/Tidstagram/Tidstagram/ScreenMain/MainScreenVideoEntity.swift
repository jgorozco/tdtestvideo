
import Foundation


class VideoEntity:NSObject,Codable{
    var id:String = ""
    var remotePath:String = ""
    var localPath:String = ""
    var localThub:String = ""
    var title:String = ""
    var subtitle:String = ""
    
    init(id:String, title:String,subtitle:String?,remotePath:String) {
        self.id = id
        self.title = title
        self.subtitle = subtitle ?? ""
        self.remotePath = remotePath
    }
}
