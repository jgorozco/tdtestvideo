import Foundation

class MainScreenPresenter:MainScreenPresenterProtocol{
    
    var view:MainScreenViewProtocol?
    var interactor:MainScreenInteractorProtocol?
    var maxPending = -1
    
    func initPresenter() {
       view?.startLoading()
       loadVideos()
    }
    
    func managePendingVideos(pending:Int){
        if (maxPending == -1){
            maxPending = pending
        }
        
        if (pending == 0){
            view?.stopLoading()
            getNextVideo()
        }else{
            view?.showProcess(process: Float(maxPending-pending)/Float(maxPending))
        }
    }
    
    func loadVideos() {
        interactor?.loadVideos{result in
            switch result{
            case .success(let pendingVideos):
                self.managePendingVideos(pending: pendingVideos)
            case .failure(let error):
                self.view?.showError(error: error)
            }
        }
    }
    
    func getNextVideo() {
        if let actualVideo = interactor?.getNextVideo()  {
            view?.showVideo(video: actualVideo)
        } else{
            view?.showError(error: TidsError.NoVideoError)
        }
    }
    
    /*
     A good improvement could be not to use same Data object for interactor/presenter/view.
     This is not good, because data from interactor, are usually somekind different as presentation.
     But in this case with this really simple pojo and add mappers dont give any adventage at this point.
     
     Layers and responsabilities are separated so we could put at this point another data object
     and a mapper when we need.
     */
    func getAllAvailableVideos() -> [VideoEntity]?{
        return interactor?.getAllVideos()
    }
}
