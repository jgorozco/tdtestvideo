import AVKit

class VideoPlayerHelper{
    
    let playerController = AVPlayerViewController()
    var playerLayer:AVPlayerLayer?
    var player:AVPlayer?
    var playerObserver:Any?
    
    func playVideo(videoUrl:URL,container:UIView,closure: @escaping (Double,AVPlayer)->Void){
        if player != nil {
            player?.pause()
            player?.removeTimeObserver(self.playerObserver!)
            self.playerObserver = nil
            player?.replaceCurrentItem(with: AVPlayerItem(url: videoUrl))
        }else{
            player = AVPlayer(url: videoUrl)
            playerController.player = player
            playerLayer = AVPlayerLayer(player:player)
            playerLayer!.frame = container.bounds
            playerLayer?.videoGravity = .resizeAspectFill
            container.layer.addSublayer(playerLayer!)
        }
        
        self.playerObserver = player?.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: 1), queue: DispatchQueue.main) { progressTime in
            closure(progressTime.seconds,self.player!)

        }
        player?.play()
    }
    
}
