import UIKit

class MainScreenVideoCell:UICollectionViewCell{
    @IBOutlet weak var itemImage:UIImageView!
    @IBOutlet weak var label:UILabel!
}
