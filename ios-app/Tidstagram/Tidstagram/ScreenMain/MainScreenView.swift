//
//  ViewController.swift
//  Tidstagram
//
//  Created by jgo on 01/07/2019.
//  Copyright © 2019 jgo. All rights reserved.
//

import UIKit
import AVKit

class MainScreenView: UIViewController,MainScreenViewProtocol,UICollectionViewDelegate,UICollectionViewDataSource {

    var presenter:MainScreenPresenterProtocol?
    let videoHelper = VideoPlayerHelper()
    var initialContainerPosition :CGPoint?
    
    //MARK: Collection variables
    var listVideos : [VideoEntity]?
    let reuseIdentifier = "myCellView"
   
    @IBOutlet weak var textTitle:UILabel!
    @IBOutlet weak var textSubtitle:UILabel!
    @IBOutlet weak var viewExtraActionContainer:UIView!
    @IBOutlet weak var viewVideoPlayerContainer:UIView!
    @IBOutlet weak var progressBarPlayer:UIProgressView!
    @IBOutlet weak var textVideoTime:UILabel!
    @IBOutlet weak var collectionOtherVideos:UICollectionView!
    @IBOutlet weak var principalActivityIndicator:UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initGestures()
        configureView()
        presenter?.initPresenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initialContainerPosition = viewExtraActionContainer.frame.origin
    }
    
    fileprivate func configureView() {
        progressBarPlayer.layer.cornerRadius = 3
        progressBarPlayer.clipsToBounds = true
        progressBarPlayer.transform = CGAffineTransform(scaleX: 1.0, y: 3.0)
    }
    
    //MARK: GESTURE MANAGER
    fileprivate func initGestures() {
        let swipeleft = UISwipeGestureRecognizer(target: self, action: #selector(mainGestures))
        swipeleft.direction = .left
        self.view.addGestureRecognizer(swipeleft)
        
        let swipeTop = UISwipeGestureRecognizer(target: self, action: #selector(mainGestures))
        swipeTop.direction = .up
        self.view.addGestureRecognizer(swipeTop)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(mainGestures))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)

    }

    @objc func mainGestures(gesture:UISwipeGestureRecognizer){
        switch gesture.direction {
        case .left:
            nextVideo()
        case .up:
            scrollUpContainer()
        case .down:
            scrollDownContainer()
        default:
            NSLog("None thing")
        }
    }
    
    func scrollUpContainer(){
        UIView.animate(withDuration: 1.0, animations: {
            self.viewExtraActionContainer.frame = CGRect(
                x: self.viewExtraActionContainer.frame.origin.x,
                y: 80,
                width: self.viewExtraActionContainer.frame.size.width,
                height: self.viewExtraActionContainer.frame.size.height)
        })
    }
    
    func scrollDownContainer(){
        UIView.animate(withDuration: 1.0, animations: {
            self.viewExtraActionContainer.frame = CGRect(
                x: self.viewExtraActionContainer.frame.origin.x,
                y: self.initialContainerPosition?.y ?? 0,
                width: self.viewExtraActionContainer.frame.size.width,
                height: self.viewExtraActionContainer.frame.size.height)
        })
    }
    
    //MARK: Collection Stuff
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showVideo(video: listVideos![indexPath.item])
        scrollDownContainer()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listVideos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MainScreenVideoCell
        
        let actualVideo = listVideos![indexPath.item]
        do {
        try cell.itemImage.image = UIImage(data: NSData(contentsOfFile: actualVideo.localThub) as Data)
        }catch{
            NSLog("Error in image!!!%@",error.localizedDescription)
        }
        cell.layer.cornerRadius = 12
        cell.label.text = actualVideo.title
        return cell
    }
    
    func nextVideo(){
        presenter?.getNextVideo()
    }
    
    func showVideos(){
        listVideos = presenter?.getAllAvailableVideos()
        collectionOtherVideos.reloadData()
    }
    
    func startLoading(){
        principalActivityIndicator.startAnimating()
    }
    
    func showProcess(process:Float){
        DispatchQueue.main.async {
            if self.viewIsActive(){
                self.textTitle.text = "Downloading videos"
                self.progressBarPlayer.progress = process
            }
        }
    }
    func stopLoading(){
        DispatchQueue.main.async {
            if self.viewIsActive(){
                self.principalActivityIndicator.stopAnimating()
                self.progressBarPlayer.progress = 0.0
                self.textTitle.text = ""
                self.showVideos()
            }
        }
    }
    func showError(error:Error){
        DispatchQueue.main.async {
            if self.viewIsActive() {
                self.principalActivityIndicator.stopAnimating()
                self.textTitle.text = "Error, please reopen app and try again"
                let alert = UIAlertController(title:"Error", message: error.localizedDescription, preferredStyle: .alert)
                self.present(alert,animated: true)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3){
                    alert.dismiss(animated: true)
                }
            }
        }
    }
    
    func showVideo(video:VideoEntity){
         DispatchQueue.main.async {
            if self.viewIsActive(){
                self.textTitle.text = video.title
                self.textSubtitle.text = video.subtitle
                let onUpdateClosure:(Double,AVPlayer)->() = { (progress,player) in
                    if let duration = player.currentItem?.duration.seconds {
                        let progress:Float = Float(progress)/Float(duration)
                        DispatchQueue.main.async {
                            self.textVideoTime.text = duration.getTimeString
                            self.progressBarPlayer.progress = progress
                        }
                        if (progress>=1.00){
                            player.seek(to: CMTime.zero)
                            player.play()
                        }
                    }
                }
                
                self.videoHelper.playVideo(videoUrl: URL.init(fileURLWithPath: video.localPath),
                 container: self.viewVideoPlayerContainer,
                 closure:onUpdateClosure)
            }
        }
    }
    
    fileprivate func viewIsActive()->Bool{
        return self.navigationController != nil
    }
    
}

