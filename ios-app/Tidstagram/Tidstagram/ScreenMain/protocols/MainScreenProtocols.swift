//
//  MainScreenProtocols.swift
//  Tidstagram
//
//  Created by jgo on 02/07/2019.
//  Copyright © 2019 jgo. All rights reserved.
//

import Foundation
import UIKit 


protocol MainScreenViewProtocol:class{
    var presenter:MainScreenPresenterProtocol? {get set}
    func startLoading()
    func showProcess(process:Float)
    func stopLoading()
    func showError(error:Error)
    func showVideo(video:VideoEntity)
}

protocol MainScreenPresenterProtocol:class{
    var view:MainScreenViewProtocol? {get set}
    var interactor:MainScreenInteractorProtocol? {get set}
    func initPresenter()
    func loadVideos()
    func getNextVideo()
    func getAllAvailableVideos() -> [VideoEntity]?
}

protocol MainScreenInteractorProtocol:class{
    var videoService:VideoServiceProtocol? {get set}
    func loadVideos(interactorCompletion:@escaping(Result<Int,Error>)->Void)
    func getNextVideo()->VideoEntity?
    func getAllVideos()->[VideoEntity]?
}
