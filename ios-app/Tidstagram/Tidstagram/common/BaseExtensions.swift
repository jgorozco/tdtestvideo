import Foundation

extension Double {
    var getTimeString:String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute,.second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        formatter.maximumUnitCount = 0
        return formatter.string(from: TimeInterval(self))!
    }
}

extension String {
    var getVideoFilename:String{
        return "\(self).mp4"
    }
    
    var getThubnailFilename:String{
        return "\(self).jpg"
    }
}
