enum TidsError:Error{
    case UnknownError
    case NetworkError
    case NoVideoError
    case pushInfoError
    case parserError
    case timeoutError
}
