import Foundation

class VideoDataMapper{

    fileprivate func convertToDic(contentText:String)->[String:Any]?{
    if let data = contentText.data(using: .utf16){
        do{
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
        }catch{
            print(error.localizedDescription)
        }
    }
    return nil
}

func parseVideo(content:String)->[VideoEntity]{
    var entities = [VideoEntity]()
    let dictToParse = convertToDic(contentText: content)?["playlist"] as! [String:Any]
    for key in dictToParse.keys{
        let actualObject = dictToParse[key] as! [String:String]
        entities.append(VideoEntity.init(id: key,
                                         title: actualObject["title"] ?? "",
                                         subtitle: actualObject["subtitle"],
                                         remotePath: actualObject["url"] ?? ""))
    }
    return entities
}

}
