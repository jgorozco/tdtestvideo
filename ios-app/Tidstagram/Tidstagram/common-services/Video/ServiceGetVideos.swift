import Foundation
import AVKit

class ServiceGetVideo:VideoServiceProtocol{
    let jsonFile = "availableVideos.json"
    let infoNotificationMock = """
        {
            "playlist": {
                "f61c3d7f92b199ed7f5812781ccba06f": {
                    "title": "Superb sunset - Miami Beach 2017",
                    "url": "https://static.xtrea.mr/media/sample1.mp4"
                },
                "05747156e369eff34cbecb40a2adda6a": {
                    "title": "Unicorns and horses spining around",
                    "url": "https://static.xtrea.mr/media/sample2.mp4"
                },
                "9f49077a912f958530060272d6d60d30": {
                    "title": "Sam Smith, Normani - Dancing With A Stranger",
                    "subtitle": "Official video (Ft. Normani)",
                    "url": "https://static.xtrea.mr/media/sample3.mp4"
                },
                "f3058e6662927f1fcd17840469cfd5f3": {
                    "title": "Tulips",
                    "url": "https://static.xtrea.mr/media/sample4.mp4"
                },
                "27da99954cbcf488fb02a145079f3bb6": {
                    "title": "Big Buck Bunny Movie",
                    "subtitle": "Animated movie yeah",
                    "url": "https://static.xtrea.mr/media/sample2.mp4"
                }
            }
        }
        """
    
    let mapper = VideoDataMapper()
    let baseDirUrl = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
    
    init() {
        
    }
    
    func getVideoCachePath(video:VideoEntity) -> String? {
        let fileUrl = self.baseDirUrl.first!.appendingPathComponent(video.id.getVideoFilename)
        if (FileManager.default.fileExists(atPath: fileUrl.path)){
            return fileUrl.path
        }else{
            return nil
        }
    }
    
    func getVideoCacheThub(video:VideoEntity) -> String? {
        let fileUrl = self.baseDirUrl.first!.appendingPathComponent(video.id.getThubnailFilename)
        if (FileManager.default.fileExists(atPath: fileUrl.path)){
            return fileUrl.path
        }else{
            return nil
        }
    }
    
    //MOCK, load from filepath, but store into disk and load from there
    func loadFromNotification(pushInfo:String? = nil) -> [VideoEntity]{
        if pushInfo != nil{
            saveInfo(data:pushInfo!)
        }else{
            saveInfo(data: infoNotificationMock)
        }
        return getVideoList()
    }
    
    func cacheVideos(videos: [VideoEntity],completion:@escaping(Result<Int,Error>)->Void){
        var videoCountAvailable = Int(videos.count)
        for videoInfo in videos{
            if (getVideoCachePath(video: videoInfo)==nil){
                downloadVideo(videoInfo:videoInfo){ result in
                    switch result{
                    case .success(_):
                        videoCountAvailable -= 1
                        completion(.success(videoCountAvailable))
                        self.getThubnailIfApply(video:videoInfo)
                    case .failure(_):
                        completion(.failure(TidsError.NetworkError))
                    }
                }
            }else{
                getThubnailIfApply(video:videoInfo)
                videoCountAvailable -= 1
                completion(.success(videoCountAvailable))
            }
        }
    }
    
    fileprivate func getThubnailIfApply(video: VideoEntity) {
        if (self.getVideoCacheThub(video: video)==nil){
            self.downloadThubnail(videoInfo: video)
        }
    }
    
    fileprivate func downloadVideo(videoInfo:VideoEntity,completion:@escaping(Result<Bool,Error>)->Void) {
        let url = URL.init(string: videoInfo.remotePath)
        URLSession.shared.dataTask(with: url!) { (data, respnse, error) in
            guard error == nil else{
                completion(.failure(TidsError.NetworkError))
                return
            }
            let finalVideoUrl = self.baseDirUrl.first!.appendingPathComponent(videoInfo.id.getVideoFilename)
            FileManager.default.createFile(atPath: finalVideoUrl.path, contents: data, attributes: nil)
            completion(.success(true))
        }.resume()
    }
    
    
    fileprivate func downloadThubnail(videoInfo:VideoEntity){
        if let videoLocalUrl = getVideoCachePath(video: videoInfo){
            let asset = AVURLAsset(url: URL.init(fileURLWithPath: videoLocalUrl))
            let assetIg = AVAssetImageGenerator(asset: asset)
            assetIg.appliesPreferredTrackTransform = true
            assetIg.apertureMode = AVAssetImageGenerator.ApertureMode.encodedPixels
            let cmTime = CMTime(seconds: 5, preferredTimescale: 60)
            assetIg.generateCGImagesAsynchronously(forTimes: [NSValue(time:cmTime)], completionHandler:{_,image,_,_,_ in
                if (image != nil ){
                     let finalThub = self.baseDirUrl.first!.appendingPathComponent(videoInfo.id.getThubnailFilename)
                     let uiImage = UIImage(cgImage: image!)
                    guard let data = uiImage.jpegData(compressionQuality: 1) else {return}
                    FileManager.default.createFile(atPath: finalThub.path, contents: data, attributes:nil)
                }
            } )
        }else{
            NSLog("Not local video yet")
        }
    }
    
    
    fileprivate func saveInfo(data:String){
        if let dir = baseDirUrl.first{
            do{
                try data.write(to: dir.appendingPathComponent(jsonFile), atomically: true, encoding:.utf8)
            }catch{
                NSLog("Storing error")
            }
        }
    }
    
    fileprivate func getVideoList() -> [VideoEntity]{
        if let dir = baseDirUrl.first{
            do{
                let content = try String(contentsOf: dir.appendingPathComponent(jsonFile), encoding: .utf8)
                return mapper.parseVideo(content:content)
            } catch{
                return [VideoEntity]()
            }
        }else{
            return [VideoEntity]()
        }
    }

}
