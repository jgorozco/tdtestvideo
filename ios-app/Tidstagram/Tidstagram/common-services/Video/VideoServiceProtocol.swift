
protocol VideoServiceProtocol:class{
    func getVideoCachePath(video:VideoEntity) -> String?
    func getVideoCacheThub(video:VideoEntity) -> String?
    func loadFromNotification(pushInfo:String?) -> [VideoEntity]
    func cacheVideos(videos: [VideoEntity],completion:@escaping(Result<Int,Error>)->Void)
}
