import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
//    let videoService = ServiceGetVideo()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        managePushReceived()
        initPrincipalView()
   //     registerPushNotifications()
        return true
    }
    
    fileprivate func initPrincipalView(){
        let mainView = MainScreenRouter.createModule()
        
        let baseNavController = UINavigationController()
        baseNavController.viewControllers = [mainView]
        
        window = UIWindow(frame: CGRect(origin: CGPoint(x: 0, y: -130),
                                        size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height+130)))
        window?.rootViewController = baseNavController
        window?.makeKeyAndVisible()
    }

    //MARK: notification suff
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    fileprivate func managePushReceived(){
        //TODO manage and test remote notification
        /*
         I will left to last part notification management because, for proper testing it needed a device, dev program account,
         application for mock the notification and all certificates. For testing purpose i will take in account that
         information will be receive by a push and store in
         
         
         videoService.loadFromNotification(pushInfo: apn.info)
         
         try to implement here
         https://developer.apple.com/documentation/foundation/url_loading_system/downloading_files_in_the_background
         if time
         */
    }
    
    fileprivate func registerPushNotifications(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]){
            [weak self] granted, error in
            guard granted else { return }
            self?.goToNotificationSettings()
        }
    }
    
    fileprivate func goToNotificationSettings(){
        UNUserNotificationCenter.current().getNotificationSettings(){settings in
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
    
}

