#  IOS App

For develop this app I use Xcode 10.2.1 and swift 5. Creation of this app don't need any external library so is not necessary to use CocoaPods or Carthage to make easier the build  and easy to maintain because have less external dependencies (and application size is much smaller. That's helps to ![improve downloads](https://segment.com/blog/mobile-app-size-effect-on-downloads/)).

## Considerations

I see some details that could make this app impossible to develop (considering time and personal situation, such as no free time to make all things I want to develop and no physical device to test), so I made some decisions to build a functional product, prepare for implement rest of features and have an app that could cache the videos and see it offline. 

- *modify the actual playlist to a well formatted json:* make a custom formatter could is a long task with many possibles errors to manage. I think its easier to adapt data to standard formats. In any other case the only think needed to make change to any file format is a mapper function. 

- *Mock notification information:* I could not get any physical device to test notifications (And in case of make this app based on a notification, for show it for you I will need to add some of your devices to my developer account or you will need to get deviceID from log and use tools such as Pusher to send APS info). I think is easier to mock this information and create structure to make easier to link notification info to actual flow.

-*Code is separated but there is no test. :-( :* I would like to make some test in interactor, presenter and view, but I prefer to make extra functionality 

-*I was some time without create an iOS app:* so there are some details about swift 4 and 5 that I was learning this week. I have as a personal goal refresh my iOS skills this summer but this put me on the go really fast :-D. As I said, I was working more in Android last years but I like work in every tech that helps me to make cools things. This is the reason because I really like python and Django and make some things in Unity 3D (for example). 

## About code

I try to make an architecture based on features. With MainScreen feature and some commons things such as videoService and extensions. All based on protocols that helps to implements other protocols (making other implementations or mocks for tests). Its based on VIPER, but is not a pure implementation. There are some trade-offs about code such as, not having different models for domain and presentation that I get to try to focus in features.

I think that the more important thing for an app code, over any architecture or name is SOLID principles. I try to follow them separating some responsibilities as video playing, video cache and download... I think there are more things that could be extracted, as a gesture management and CollectionController on MainScreen, or separate json video management and video cache and update management. Probably in next iteration ;-)

Design is important and I try to adapt app as close as I could make to sketch, unfortunately not every detail is in app, but I include custom fonts (monstserrat) and other design details (I know how important is typography for designers). I add an icon (quite simple) to differentiate in app list. 

## Pending details.

- Video Download have some pending improvements. For example when failing, notify to presenter-view and put a message to re open the app. Some videos could be downloaded but not showing in the app. This could be easy to change modify the way of notify it and returning only available videos is easy to do with a filter. 

- Image video caching is quite simple but if an image fails, app keeps working showing video title in cell. I think videos are important for app but app could work with title in cell. Of course I could implement a better system where cells update image when cache, and retry to get if does get. Maybe I will do in next release (its a great opportunity to learn and challenge yourself this kind of apps ;-) )

- Notification stuff. Is some kind of prepared, with appDelegate functions and some comments, ServiceGetVideos is prepared to call from appDelegate and store the Json and there is a way to try to download videos in background. With more time and a physical device I could implement it. 

- Test, are really important to keep code quality and don't make stupid mistakes, I would like to make tests for interaction (really important part of app) and presenter. I think error management is something that a great set of test will help. There are some code comments in this part about it. 

## Build instructions and test

Its necessary Xcode 10.2.1, download project and import it to Xcode. Information with playlist is mocked on ServiceGetVideos, but function called is prepared for receive info from push and stored.

### Preconditions

- Mac with last OS version installed
- Xcode updated to last version.

### Instructions
- Open Xcode
- In Welcome Screen there is an option called "Clone an existing project"
- type or paste repository URL (https://jgorozco@bitbucket.org/jgorozco/tdtestvideo.git) and click "Clone"
- select a folder where clone all project, when finish click done
- when finish the project will open in Xcode
-- If not, open again Xcode and click in Welcome Screen, in bottom-right, "Open another project" and go to repository folder/ios-app. You have to open the directory with the .xcodeproj or this file.
- * At this point you have to see all project code*
- In the top-left there are a play button and next, a project+device selector. Select an iPhone type (such as iphone7) and click play.
- The app must appear in an iPhone simulator. 
- First load show a loading with a bar that inform user that videos are downloading. When finish, began to play first video.
- User could swipe left to go next video, and auto play, or swipe up to show list of videos. When click in one of them, autoplay and close it 
- When open again recheck all videos are downloaded and show first to play. 



